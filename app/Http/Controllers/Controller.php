<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function saveFile($file,$type="file")
    {
        $fileName = time() . '.' . $file->getClientOriginalName();
        $file->move('resources/assets/images/user_image/', $fileName);
        return '/resources/assets/images/user_image/'.$fileName;
    }

    public static function returnDateTimeFormat($date)
    {
        $time=strtotime($date);
        return date("Y-m-d H:i:s",$time);
    }

    public static function sendPushNotification($data, $to, $options,$notification=null)
    {
        $apiKey =env('PUSHY_KEY');
        $post = $options ?: array();
        $post['to'] = $to;
        $post['data'] = $data;
        if($notification!=null){
            $post['notification'] = $notification;
        }
        $headers = array(
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.pushy.me/push?api_key=' . $apiKey);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post, JSON_UNESCAPED_UNICODE));
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo curl_error($ch);
        }
        return $result;

    }

    public static function returnDateFormat($date)
    {
        $time=strtotime($date);
        return date("Y-m-d",$time);
    }

    public static function getNotificationMessage()
    {
      return  DB::table("notification_message")->select("*")->get()->keyBy("key");
    }

    public static  function sendSms($message,$mobile)
    {
        $post="msg=".urlencode($message)."&number=".$mobile."&key=".env('SMS_API_KEY')."&dezsmsid=".env('DEZSMS_ID')."&senderid=".env('SENDER_ID')."&xcode=965";
        return  self::__curl($post);
    }
    public static function __curl($post){
        try{
            $ch = curl_init(env('API_SMS_URL'));
            curl_setopt ($ch, CURLOPT_POST, 1);
            curl_setopt ($ch, CURLOPT_POSTFIELDS,  $post);
            curl_setopt ($ch, CURLOPT_RETURNTRANSFER , 1);
            $result= curl_exec ($ch);
            curl_close ($ch);
            unset($ch);
            if($result==100){
                unset($result);
                return true;
            }
            return false;
        }catch (\Exception $exception){
            return false;
        }

    }

    public function makeSocialImage($post)
    {
        $path="/images/template_image.jpeg";
        $id=3;



        $image=File::get(public_path($path));
        $p=explode("/",$path);
        $name=end($p);
        array_pop($p);
        $basePath=implode('/',$p);
        $midImageUrl =$basePath.'/'.$id.'_post_'.$name;
        $img=\Image::make($image)->resize(1000,1000);
        $text="this is window this is  szdmkmd skdlkjsk skdjsk dsdmskk m dsdksd skdjskd \n sdksjdkmsd skdksd sdjksjd \n sdskdks skdjsk sdkjksd skdjksd sdksjd sdksjdkjskdj asdasd asdasd asdasdasd asdasdsad asdasdas sadd eeeeeeeend ";

        $width       =1000 ;
        $height      = 800;
        $center_x    = $width / 2;
        $center_y    = $height / 2;
        $max_len     = 90;
        $font_size   = 17;
        $font_height=12;
        $lines = explode("\n", wordwrap($text, $max_len));
        $y     = $center_y - ((count($lines) - 1) * $font_height);

        $img->text('For rent yyyyyy in xxxxx ', 500,300, function($font) {
            $font->file(public_path('fonts/Helvetica-Font/Helvetica-Bold.ttf'));
            $font->size(18);
            $font->align('center');
            $font->valign('center');
            $font->angle(0);
        });
        foreach ($lines as $line) {
            $img->text($line, $center_x, $y, function ($font)use ($font_size) {
                $font->file(public_path('fonts/Helvetica-Font/Helvetica-Bold.ttf'));
                $font->size($font_size);
                $font->align('center');
                $font->valign('center');
                $font->angle(0);
            });
            $y += $font_height * 2;
        }

        $img->text('66444569 ',500,600, function($font) {
            $font->file(public_path('fonts/Helvetica-Font/Helvetica-Bold.ttf'));
            $font->size(17);
            $font->align('center');
            $font->valign('center');
            $font->angle(0);
        });
        $img->save(public_path($midImageUrl));

        dd($midImageUrl);
    }




}
