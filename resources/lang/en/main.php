<?php


return [
    'sms_code_is_not_valid' => 'sms code is not valid',
    'user_not_found' => 'user not found',
    'not_exist_combination'=>'user not found',
    "need_login"=>"No valid authentication for user",
    "not_exist_user"=>"user not found",
    "not_active_user"=>"User is not Active",
    "password_update_successfully"=>"Your Password Update SuccessFully",
    "old_password_is_incorrect"=>"Old Password Is Incorrect",
    "invalid_user"=>"Invalid User!",
    "success_profile_update"=>"Your Profile Update SuccessFully",
    "register_success"=>"Register SuccessFully",

];
