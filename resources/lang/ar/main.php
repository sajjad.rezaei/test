<?php

return [
    'sms_code_is_not_valid' => 'كود الرسائل القصيرة غير صالح',
    'user_not_found' => 'المستخدم ليس موجود',
    'not_exist_combination'=>'المستخدم ليس موجود',
    "need_login"=>"لا مصادقة صالحة للمستخدم",
    "not_exist_user"=>"المستخدم ليس موجود",
    "not_active_user"=>"المستخدم غير نشط",
    "password_update_successfully"=>"تم تحديث كلمة المرور الخاصة بك بنجاح",
    "old_password_is_incorrect"=>"كلمة سر قديمة ليست صحيحة",
    "invalid_user"=>"مستخدم غير صالح!",
    "success_profile_update"=>"تم تحديث ملف التعريف الخاص بك بنجاح",
    "register_success"=>"تسجيل بنجاح",
];
